import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f



########################
### DEFINE THE CURVE ###
########################

# The dim_p
dim_p = 1
# Bezier control points
control_pts = numpy.array([[0.,3.,1.,1.],
                           [1.,1.,2.,0.],
                           [0.,1.,2.,3.]])
# The knot vector
knot_v = numpy.array([[0,0,0,0,1,1,1,1]])
# The order
p = numpy.array([3])
# The spans
spans = numpy.array([len(control_pts[0,:])-1])
# The spatial dimension of the output
dim_s = 3
# Resolution
res = numpy.array([201])


######################
### PLOT THE CURVE ###
######################

# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the bezier curve
X, T = b_spline( dim_p=dim_p,
                 control_pts=control_pts,
                 order=p,
                 spans=spans,
                 dim_s=dim_s,
                 knot_v=knot_v,
                 res=res)



# Extract the control points' x and y coordinates
px = control_pts[0]
py = control_pts[1]
pz = control_pts[2]

# Draw the curve
fig = pyplot.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot(px, py, zs=pz, zdir='z',color='r')
ax.scatter(X[0],X[1],zs=X[2],zdir='z')
