import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f

# The knot vector
knot_v = numpy.array([[0,0,0,0,1,1,2,2,3,3,4,4,4,4]])
# The order
order = numpy.array([3])
p = order

h = 0.01
t = numpy.arange(numpy.min(knot_v[0]), numpy.max(knot_v[0])-0.99*h, h)

# Plot the basis functions
pyplot.figure()
for i in range(0,len(knot_v[0])):
    #pyplot.figure()
    pyplot.plot(t,bspline_basis_f(i,p[0],knot_v[0],t))
    pyplot.axis([min(knot_v[0]),max(knot_v[0]),-0.5,1.5])
    print("Ploting..",i)
