import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f
write_obj_file = ssnurbs.write_obj_file



########################
### DEFINE THE CURVE ###
########################

# The dim_p
dim_p = 2
# Bezier control points
control_pts = numpy.array([ [[ 0., 1., 1., 1., 0.,-1.,-1.,-1., 0.],
                            [ 0., 2., 2., 2., 0.,-2.,-2.,-2., 0.]],
                           [[-1.,-1., 0., 1., 1., 1., 0.,-1, -1.],
                            [-2.,-2., 0., 2., 2., 2., 0.,-2, -2.]],
                           [[ 4., 4., 4., 4., 4., 4., 4., 4., 4.],
                            [ 4., 4., 4., 4., 4., 4., 4., 4., 4.]] ])
"""
control_pts = numpy.array([ [[0.,1.,2.,3.,4., 5., 6., 7.,8.],
                            [0.,1.,2.,3.,4., 5., 6., 7.,8.]],
                           [[0.,0.,0.,0.,0., 0., 0., 0.,0 ],
                            [0.,0.,0.,0.,0., 0., 0., 0.,0 ]],
                           [[0.,0.,0.,0.,0., 0., 0., 0.,0.],
                            [1.,1.,1.,1.,1., 1., 1., 1.,1.]] ])
"""

# Weights
arc_angle = numpy.pi/2.
weights = numpy.array([[1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                        1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.],
                       [1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                        1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.]])

"""
weights = numpy.array( [[1.,1.,1.,1.,1.,1.,1.,1.,1.],
                        [1.,1.,1.,1.,1.,1.,1.,1.,1.]] )
"""
# The knot vector
knot_v = numpy.array([[0,0,1,1],[0,0,0,1,1,2,2,3,3,4,4,4]])
# The order (order of n-rows-direction, order of n-cols-dim_s, etc)
p = numpy.array([1,2])
# The spans
spans = numpy.array([ len(control_pts[0,:,0])-1,  len(control_pts[0,0,:])-1 ])
# The dim_s of the output
dim_s = 3
# Resolution
res = numpy.array([61,61])


#######################
### BUILD THE CURVE ###
#######################

# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the bezier curve
X, T = nurbs_spline( dim_p=dim_p,
                     control_pts=control_pts,
                     weights=weights,
                     order=p,
                     spans=spans,
                     dim_s=dim_s,
                     knot_v=knot_v,
                     res=res)

#################################
#### Plot the basis functions ###
#################################
"""
dim = 1
# Plot the basis functions
pyplot.figure()
for i in range(0,len(knot_v[dim])):
    #pyplot.figure()
    pyplot.plot(T[dim],bezier_basis_f(i,p[dim],knot_v[dim],T[dim]),'.')
    pyplot.axis([min(knot_v[dim]),max(knot_v[dim]),-0.5,1.5])
"""

######################
### PLOT THE CURVE ###
######################


# Extract the control points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]
pz = control_pts[2,:]

# Draw the curve
fig = pyplot.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(0,len(control_pts[0,:,0])):
    ax.plot(px[i,:], py[i,:], zs=pz[i,:], zdir='z')
for i in range(0,len(control_pts[0,0,:])):
    ax.plot(px[:,i], py[:,i], zs=pz[:,i], zdir='z')
ax.scatter(X[0],X[1],zs=X[2],zdir='z')

write_obj_file("output/plot_nurbs_surf3D.obj",[X])
