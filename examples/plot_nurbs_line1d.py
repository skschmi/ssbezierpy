import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f


########################
### DEFINE THE CURVE ###
########################


# The dim_p
dim_p = 1
# control points
control_pts = numpy.array([[0.,1.],
                          [0.,1.]])
# Weights
weights = numpy.array([1.,1.])
# The knot vector
knot_v = numpy.array([[0,0,1,1]])
# The order
p = numpy.array([1])
# The spans
spans = numpy.array([len(control_pts[0,:])-1])
# The dim_s of the output
dim_s = 2
# Resolution
res = numpy.array([201])



######################
### PLOT THE CURVE ###
######################

# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the nurbs curve
X, T = nurbs_spline( dim_p=dim_p,
                     weights=weights,
                     control_pts=control_pts,
                     order=p,
                     spans=spans,
                     dim_s=dim_s,
                     knot_v=knot_v,
                     res=res)

# Extract the control points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]

# Plot the basis functions
pyplot.figure()
for i in range(0,len(knot_v[0])):
    #pyplot.figure()
    pyplot.plot(T[0],bspline_basis_f(i,p[0],knot_v[0],T[0]))
    pyplot.axis([min(knot_v[0]),max(knot_v[0]),-0.5,1.5])

# Draw the curve
import matplotlib.lines as lines
fig, ax = pyplot.subplots()
fig.set_size_inches(6,6)          # Make graph square
max_wid = max((max(px)-min(px)),(max(py)-min(py))) + 1.0
pyplot.axis([min(px)-0.5,min(px)+max_wid,min(py)-0.5,min(py)+max_wid])
ax.set_autoscale_on(False)
ax.add_line(pyplot.Line2D(px, py, linewidth=2, color='red'))
ax.add_line(pyplot.Line2D(X[0], X[1], linewidth=2, color='blue'))
ax.plot()
