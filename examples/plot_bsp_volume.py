import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f
write_obj_file_volumes = ssnurbs.write_obj_file_volumes



########################
### DEFINE THE CURVE ###
########################

# The dim_p
dim_p = 3
# Bezier control points
control_pts = numpy.array([ [[[0.,1.5,3.5,5.],
                             [0.,1.5,3.5,5.],
                             [0.,1.5,3.5,5.],
                             [0.,1.5,3.5,5.]],
                            [[1.,2.,3.,4.],
                             [1.,2.,3.,4.],
                             [1.,2.,3.,4.],
                             [1.,2.,3.,4.]],
                            [[1.,2.,3.,4.],
                             [1.,2.,3.,4.],
                             [1.,2.,3.,4.],
                             [1.,2.,3.,4.]],
                            [[0.,1.5,3.5,5.],
                             [0.,1.5,3.5,5.],
                             [0.,1.5,3.5,5.],
                             [0.,1.5,3.5,5.]]],

                           [[[0.,0.,0.,0.],
                             [1.5,1.5,1.5,1.5],
                             [3.5,3.5,3.5,3.5],
                             [5.,5.,5.,5.]],
                            [[1.,1.,1.,1.],
                             [2.,2.,2.,2.],
                             [3.,3.,3.,3.],
                             [4.,4.,4.,4.]],
                            [[1.,1.,1.,1.],
                             [2.,2.,2.,2.],
                             [3.,3.,3.,3.],
                             [4.,4.,4.,4.]],
                            [[0.,0.,0.,0.],
                             [1.5,1.5,1.5,1.5],
                             [3.5,3.5,3.5,3.5],
                             [5.,5.,5.,5.]]],

                           [[[0.,0.,0.,0.],
                             [0.,0.,0.,0.],
                             [0.,0.,0.,0.],
                             [0.,0.,0.,0.]],
                            [[1.,1.,1.,1.],
                             [1.,1.,1.,1.],
                             [1.,1.,1.,1.],
                             [1.,1.,1.,1.]],
                            [[2.,2.,2.,2.],
                             [2.,2.,2.,2.],
                             [2.,2.,2.,2.],
                             [2.,2.,2.,2.]],
                            [[3.,3.,3.,3.],
                             [3.,3.,3.,3.],
                             [3.,3.,3.,3.],
                             [3.,3.,3.,3.]]]  ])



# The knot vector
knot_v = numpy.array([[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1]])
# The order
p = numpy.array([3,3,3])
# The spans
spans = numpy.array([ len(control_pts[0,:,0,0])-1, len(control_pts[0,0,:,0])-1, len(control_pts[0,0,0,:])-1 ])
# The spatial dimension of the output
dim_s = 3
# Resolution
#res = numpy.array([16,16,16])
res = numpy.array([10,10,10])

######################
### PLOT THE CURVE ###
######################

# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the bezier curve
X, T = b_spline( dim_p=dim_p,
                 control_pts=control_pts,
                 order=p,
                 spans=spans,
                 dim_s=dim_s,
                 knot_v=knot_v,
                 res=res)



# Extract the control points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]
pz = control_pts[2,:]

# Draw the curve
fig = pyplot.figure()
ax = fig.add_subplot(111, projection='3d')

for i in range(0,len(control_pts[0,:,0,0])):
    for j in range(0,len(control_pts[0,0,:,0])):
        ax.plot(px[i,j,:], py[i,j,:], zs=pz[i,j,:], zdir='z')

for i in range(0,len(control_pts[0,:,0,0])):
    for j in range(0,len(control_pts[0,0,0,:])):
        ax.plot(px[i,:,j], py[i,:,j], zs=pz[i,:,j], zdir='z')

for i in range(0,len(control_pts[0,0,:,0])):
    for j in range(0,len(control_pts[0,0,0,:])):
        ax.plot(px[:,i,j], py[:,i,j], zs=pz[:,i,j], zdir='z')

ax.scatter(X[0],X[1],zs=X[2],zdir='z')


write_obj_file_volumes("output/plot_bsp_volume.obj",[X])
