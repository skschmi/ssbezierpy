import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f
write_obj_file_volumes = ssnurbs.write_obj_file_volumes


########################
### DEFINE THE CURVE ###  Top of Pipe
########################

# The dim_p
dim_p = 3
# Bezier control points
control_pts = numpy.array([ [[[ 0., 1., 1., 1., 0.,-1.,-1.,-1., 0.],
                             [ 0., 2., 2., 2., 0.,-2.,-2.,-2., 0.]],
                            [[ 0., 1., 1., 1., 0.,-1.,-1.,-1., 0.],
                             [ 0., 2., 2., 2., 0.,-2.,-2.,-2., 0.]],
                            [[ 0., 1., 1., 1., 0.,-1.,-1.,-1., 0.],
                             [ 0., 2., 2., 2., 0.,-2.,-2.,-2., 0.]]],

                           [[[-1.,-1., 0., 1., 1., 1., 0.,-1, -1.],
                             [-2.,-2., 0., 2., 2., 2., 0.,-2, -2.]],
                            [[-1.,-1., 0., 1., 1., 1., 0.,-1, -1.],
                             [-2.,-2., 0., 2., 2., 2., 0.,-2, -2.]],
                            [[-1.,-1., 0., 1., 1., 1., 0.,-1, -1.],
                             [-2.,-2., 0., 2., 2., 2., 0.,-2, -2.]]],

                           [[[ 8., 8., 8., 8., 8., 8., 8., 8., 8.],
                             [ 8., 8., 8., 8., 8., 8., 8., 8., 8.]],
                            [[6.5,6.5,6.5,6.5,6.5,6.5,6.5,6.5,6.5],
                             [6.5,6.5,6.5,6.5,6.5,6.5,6.5,6.5,6.5]],
                            [[ 5., 5., 5., 5., 5., 5., 5., 5., 5.],
                             [ 5., 5., 5., 5., 5., 5., 5., 5., 5.]]] ])



# Weights
arc_angle = numpy.pi/2.
weights = numpy.array([[[1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.],
                        [1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.]],

                       [[1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.],
                        [1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.]],

                       [[1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.],
                        [1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.]] ])


# The knot vector
knot_v = numpy.array([[0,0,0,1,1,2,2,2],[0,0,1,1],[0,0,0,1,1,2,2,3,3,4,4,4]])
# The order (order of n-rows-direction, order of n-cols-dimension, etc)
p = numpy.array([2,1,2])
# The spans
spans = numpy.array([ len(control_pts[0,:,0,0])-1, len(control_pts[0,0,:,0])-1,  len(control_pts[0,0,0,:])-1 ])
# The dim_s of the output
dim_s = 3
# Resolution
res = numpy.array([21,11,51])
#res = numpy.array([3,3,11])

#######################
### BUILD THE CURVE ###
#######################

# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the bezier curve
X, T = nurbs_spline( dim_p=dim_p,
                     control_pts=control_pts,
                     weights=weights,
                     order=p,
                     spans=spans,
                     dim_s=dim_s,
                     knot_v=knot_v,
                     res=res)

X1 = X
#################################
#### Plot the basis functions ###
#################################
"""
dim = 1
# Plot the basis functions
pyplot.figure()
for i in range(0,len(knot_v[dim])):
    #pyplot.figure()
    pyplot.plot(T[dim],bezier_basis_f(i,p[dim],knot_v[dim],T[dim]),'.')
    pyplot.axis([min(knot_v[dim]),max(knot_v[dim]),-0.5,1.5])
"""

######################
### PLOT THE CURVE ###
######################


# Extract the control points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]
pz = control_pts[2,:]

# Draw the curve
fig = pyplot.figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(0,len(control_pts[0,:,0])):
    ax.plot(px[i,:].flatten(), py[i,:].flatten(), zs=pz[i,:].flatten(), zdir='z')
for i in range(0,len(control_pts[0,0,:])):
    ax.plot(px[:,i].flatten(), py[:,i].flatten(), zs=pz[:,i].flatten(), zdir='z')
ax.scatter(X[0].flatten(),X[1].flatten(),zs=X[2].flatten(),zdir='z')













########################
### DEFINE THE CURVE ###  Bottom of Pipe (TODO: adjust points to make the bent part of the pipe)
########################

# The dim_p
dim_p = 3
# Bezier control points
control_pts = numpy.array([ [[[ 0., 1., 1., 1., 0.,-1.,-1.,-1., 0.],  #Inner  #DONE  #X
                             [ 0., 2., 2., 2., 0.,-2.,-2.,-2., 0.]], #Outer  #DONE
                            [[ 0., 1., 1., 1., 0.,-1.,-1.,-1., 0.],  #Inner #DONE
                             [ 0., 2., 2., 2., 0.,-2.,-2.,-2., 0.]], #Outer #DONE
                            [[ 3., 3., 3., 3., 3., 3., 3., 3., 3.],  #Inner #DONE
                             [ 3., 3., 3., 3., 3., 3., 3., 3., 3.]]],#Outer #DONE

                           [[[-1.,-1., 0., 1., 1., 1., 0.,-1, -1.],  #Inner #DONE  #Y
                             [-2.,-2., 0., 2., 2., 2., 0.,-2, -2.]], #Outer #DONE
                            [[-1.,-1., 0., 1., 1., 1., 0.,-1, -1.],  #Inner #DONE
                             [-2.,-2., 0., 2., 2., 2., 0.,-2, -2.]], #Outer #DONE
                            [[-1.,-1., 0., 1., 1., 1., 0.,-1, -1.],   #Inner #DONE
                             [-2.,-2., 0., 2., 2., 2., 0.,-2, -2.]]], #Outer #DONE

                           [[[ 5., 5., 5., 5., 5., 5., 5., 5., 5.],  #Inner  #DONE   #Z
                             [ 5., 5., 5., 5., 5., 5., 5., 5., 5.]], #Outer  #DONE
                            [[ 2., 3., 3., 3., 2., 1., 1., 1., 2.],  #Inner  #DONE
                             [ 2., 4., 4., 4., 2., 0., 0., 0., 2.]], #Outer  #DONE
                            [[ 2., 3., 3., 3., 2., 1., 1., 1., 2.],      #Inner #DONE
                             [ 2., 4., 4., 4., 2., 0., 0., 0., 2.]]] ])  #Outer #DONE



# Weights
arc_angle = numpy.pi/2.
weights = numpy.array([[[1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.],
                        [1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.]],

                       [[1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.],
                        [1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.]],

                       [[1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.],
                        [1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.),
                         1., numpy.cos(arc_angle/2.), 1., numpy.cos(arc_angle/2.), 1.]] ])


# The knot vector
knot_v = numpy.array([[0,0,0,1,1,2,2,2],[0,0,1,1],[0,0,0,1,1,2,2,3,3,4,4,4]])
# The order (order of n-rows-direction, order of n-cols-dimension, etc)
p = numpy.array([2,1,2])
# The spans
spans = numpy.array([ len(control_pts[0,:,0,0])-1, len(control_pts[0,0,:,0])-1,  len(control_pts[0,0,0,:])-1 ])
# The dim_s of the output
dim_s = 3
# Resolution
res = numpy.array([21,11,51])


#######################
### BUILD THE CURVE ###
#######################

# dim_p, points, order, spans, dim_s, knot_v, res
# Compute the bezier curve
X, T = nurbs_spline( dim_p=dim_p,
                     control_pts=control_pts,
                     weights=weights,
                     order=p,
                     spans=spans,
                     dim_s=dim_s,
                     knot_v=knot_v,
                     res=res)

X2 = X
######################
### PLOT THE CURVE ###
######################


# Extract the points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]
pz = control_pts[2,:]

# Draw the curve
#fig = pyplot.figure()
#ax = fig.add_subplot(111, projection='3d')
for i in range(0,len(control_pts[0,:,0])):
    ax.plot(px[i,:].flatten(), py[i,:].flatten(), zs=pz[i,:].flatten(), zdir='z')
for i in range(0,len(control_pts[0,0,:])):
    ax.plot(px[:,i].flatten(), py[:,i].flatten(), zs=pz[:,i].flatten(), zdir='z')
ax.scatter(X[0].flatten(),X[1].flatten(),zs=X[2].flatten(),zdir='z', color='r')


write_obj_file_volumes("output/pipe.obj",[X1])
