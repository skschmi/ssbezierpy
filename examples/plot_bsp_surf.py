import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f



########################
### DEFINE THE CURVE ###
########################

# The dim_p
dim_p = 2
# Bezier control points
control_pts = numpy.array([ [[0.,2.,5.,7.],
                             [1.,2.,5.,6.],
                             [1.,2.,5.,6.],
                             [0.,2.,5.,7.]],
                            [[0.,4.,4.,0.],
                             [5.,5.,5.,5.],
                             [6.,7.,7.,6.],
                             [8.,10,10,8.]] ])
# The knot vector
knot_v = numpy.array([[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1]])
# The order
p = numpy.array([3,3])
# The spans
spans = numpy.array([ len(control_pts[0,:,0])-1,  len(control_pts[0,0,:])-1 ])
# The spatial dimension of the output
dim_s = 2
# Resolution
res = numpy.array([51,51])



######################
### PLOT THE CURVE ###
######################

# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the bezier curve
X, T = b_spline( dim_p=dim_p,
                 control_pts=control_pts,
                 order=p,
                 spans=spans,
                 dim_s=dim_s,
                 knot_v=knot_v,
                 res=res)



# Extract the control points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]

# Plot the basis functions
#pyplot.figure()
#for i in range(0,len(knot_v[0])):
#    #pyplot.figure()
#    pyplot.plot(ret_t[0],bezier_basis_f(i,p[0],knot_v[0],ret_t[0]))
#    pyplot.axis([min(knot_v[0]),max(knot_v[0]),-0.5,1.5])

# Draw the curve
import matplotlib.lines as lines
fig, ax = pyplot.subplots()
fig.set_size_inches(6,6)          # Make graph square
max_wid = numpy.max(((numpy.max(px)-numpy.min(px)),(numpy.max(py)-numpy.min(py)))) + 1.0
pyplot.axis([numpy.min(px)-0.5,numpy.min(px)+max_wid,numpy.min(py)-0.5,numpy.min(py)+max_wid])
ax.set_autoscale_on(False)

ax.plot(X[0], X[1],'.',color='blue')
for i in range(0,len(control_pts[0,0,:])):
    ax.add_line(pyplot.Line2D(px[i,:], py[i,:], linewidth=2, color='red'))
for i in range(0,len(control_pts[0,0,:])):
    ax.add_line(pyplot.Line2D(px[:,i], py[:,i], linewidth=2, color='red'))
ax.plot()

#ax.add_line(pyplot.Line2D(px, py, linewidth=2, color='red'))
#ax.add_line(pyplot.Line2D(ret_coords[0], ret_coords[1], linewidth=2, color='blue'))
#ax.plot()
