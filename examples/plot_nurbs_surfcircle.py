import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot

import os
import importlib.util
spec = importlib.util.spec_from_file_location("module.name", os.getcwd()+"/ssnurbs.py")
ssnurbs = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ssnurbs)
b_spline = ssnurbs.b_spline
nurbs_spline = ssnurbs.nurbs_spline
bspline_basis_f = ssnurbs.bspline_basis_f



########################
### DEFINE THE CURVE ###
########################

# The dim_p
dim_p = 2
# Bezier control points
control_pts = numpy.array([ [[-1., -1.,  0],
                             [-1.,  0,   1],
                             [0., 1.,    1.]],
                            [[0., 1.,  1.],
                             [-1, 0.,  1.],
                             [-1., -1, 0.]] ])

arc_angle = numpy.pi/2.
wC = numpy.cos(arc_angle/2.)
weights = numpy.array([[1.,wC,1.],
                       [wC,1.,wC],
                       [1.,wC,1.]])

# The knot vector
knot_v = numpy.array([[0,0,0,1,1,1],[0,0,0,1,1,1]])
# The order
p = numpy.array([2,2])
# The spans
spans = numpy.array([ len(control_pts[0,:,0])-1,  len(control_pts[0,0,:])-1 ])
# The dim_s of the output
dim_s = 2
# Resolution
res = numpy.array([101,101])


# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the NURBS surface
X, T = nurbs_spline( dim_p=dim_p,
                     control_pts=control_pts,
                     weights=weights,
                     order=p,
                     spans=spans,
                     dim_s=dim_s,
                     knot_v=knot_v,
                     res=res)

# Extract the control points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]


######################
### PLOT THE CURVE ###
######################

# Draw the curve
import matplotlib.lines as lines
fig, ax = pyplot.subplots()
fig.set_size_inches(6,6)          # Make graph square
max_wid = numpy.max(((numpy.max(px)-numpy.min(px)),(numpy.max(py)-numpy.min(py)))) + 1.0
pyplot.axis([numpy.min(px)-0.5,numpy.min(px)+max_wid,numpy.min(py)-0.5,numpy.min(py)+max_wid])
ax.set_autoscale_on(False)

ax.plot(X[0], X[1],'.',color='green')
for i in range(0,len(control_pts[0,0,:])):
    ax.add_line(pyplot.Line2D(px[i,:], py[i,:], linewidth=2, color='red'))
for i in range(0,len(control_pts[0,0,:])):
    ax.add_line(pyplot.Line2D(px[:,i], py[:,i], linewidth=2, color='red'))
ax.plot()

#ax.add_line(pyplot.Line2D(px, py, linewidth=2, color='red'))
#ax.add_line(pyplot.Line2D(ret_coords[0], ret_coords[1], linewidth=2, color='blue'))
#ax.plot()






"""
##################
##############################################
DRAWING A CIRCLE AROND THE SURFACE, to compare
##############################################
##################
"""

# The dim_p
dim_p = 1
# Bezier control points
control_pts = numpy.array([[1.,1.,0.,-1.,-1., -1., 0., 1.,1.],
                          [0.,1.,1.,1.,0.,-1.,-1.,-1.,0]])
# Weights
arc_angle = numpy.pi/2.
weights = numpy.array([1.,
                       numpy.cos(arc_angle/2.),
                       1.,
                       numpy.cos(arc_angle/2.),
                       1.,
                       numpy.cos(arc_angle/2.),
                       1.,
                       numpy.cos(arc_angle/2.),
                       1.])
# The knot vector
knot_v = numpy.array([[0,0,0,1,1,2,2,3,3,4,4,4]])
# The order
p = numpy.array([2])
# The spans
spans = numpy.array([len(control_pts[0,:])-1])
# The dim_s of the output
dim_s = 2
# Resolution
res = numpy.array([501])

# dim_p, control_pts, order, spans, dim_s, knot_v, res
# Compute the bezier curve
X, T = nurbs_spline( dim_p=dim_p,
                     control_pts=control_pts,
                     weights=weights,
                     order=p,
                     spans=spans,
                     dim_s=dim_s,
                     knot_v=knot_v,
                     res=res)

# Extract the control points' x and y coordinates
px = control_pts[0,:]
py = control_pts[1,:]

# Draw the curve
import matplotlib.lines as lines
#fig, ax = pyplot.subplots()
#fig.set_size_inches(6,6)          # Make graph square
max_wid = max((max(px)-min(px)),(max(py)-min(py))) + 1.0
pyplot.axis([min(px)-0.5,min(px)+max_wid,min(py)-0.5,min(py)+max_wid])
ax.set_autoscale_on(False)
ax.add_line(pyplot.Line2D(px, py, linewidth=2, color='red'))
ax.add_line(pyplot.Line2D(X[0], X[1], linewidth=1, color='blue'))
ax.plot()
