import numpy


# *** bspline_basis_f ***
# i: knot index
# order: The order of the curve
# knot_v: The knot vector. Example: numpy.array([0,0,0,1,2,3,3,3])
# t: The array of parameter values where the function is evaluated, valid between min(knot_vector) and max(knot_vector)
# Returns: Array of values of the basis function evaluated at locations t, the same dimension as t.
def bspline_basis_f(i,order,knot_v,t):
    ans = numpy.zeros(numpy.shape(t))
    p = order
    if (p >= 0):
        if (p == 0 and i >= 0 and i < len(knot_v)):
            ans += numpy.logical_and(numpy.greater_equal(t,knot_v[i]), numpy.less(t,knot_v[i+1]))
            return ans
        else:
            if (i >= 0 and i < len(knot_v) and i+p >= 0 and i+p < len(knot_v) and knot_v[i+p] != knot_v[i]):
                ans += ((t             - knot_v[i]) /  (knot_v[i+p]   - knot_v[i]  )) * bspline_basis_f(i  , p-1, knot_v, t)

            if (i+1 >= 0 and i+1 < len(knot_v) and i+p+1 >= 0 and i+p+1 < len(knot_v) and knot_v[i+p+1] != knot_v[i+1] ):
                ans += ((knot_v[i+p+1] - t)         /  (knot_v[i+p+1] - knot_v[i+1])) * bspline_basis_f(i+1, p-1, knot_v, t)

            return ans
    else:
        return ans



#*** b_spline ***
# dim_p: The parameter-space dimension: 1 = line, 2 = surface, 3 = volume, etc.
# control_pts: Array of control points. Indicies: i,j,k,... where i is over
#    the coordinates of each point, j,k,... are over the spacial directions of the grid.
#    j only for a line; j,k for a surface; j,k,l for a volume; etc.
# order:  The order of the curve in each direction.
# spans:  The number of spans between control points in each direction
# dim_s:  The number of spatial dimensions the output will be in. 1 = 1D, 2 = 2D, 3 = 3D, etc.
# knot_v: The array of knot vectors, for each direction.
#    Surface example: numpy.array([[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1]])
# res:  The number of intervals between the beginning and ending t, in each direction
# Returns:
#     X: Array of dimension (dim_s, res[0], res[1],...,res[n]), containing the
#          spatial coordinate locations of the samples of the spline surface.
#     T: Array of dimension (dim_p, res[0], res[1],...,res[n]), containing the
#          parameter-space coordinate locations of the samples the spline surface.
def b_spline(dim_p, control_pts, order, spans, dim_s, knot_v, res):

    # Build the meshgrid, algorithmically creating the expression depending on "dim_p".
    h = []
    t = []
    for i in range(0,dim_p):
        h.append( (numpy.max(knot_v[i]) - numpy.min(knot_v[i])) / float(res[i]-1) )
        t.append( numpy.arange(numpy.min(knot_v[i]), numpy.max(knot_v[i])+0.1*h[i], (1.0-1e-14)*h[i]) )
    if(dim_p > 1):
        command = "T = numpy.meshgrid("
        for i in range(0,dim_p):
            command += "t["+str(i)+"],"
        command += "indexing='xy')"
    else:
        command = "T = [t[0]]"
    print(command)
    _locals = locals()
    exec(command, globals(), _locals)
    T = _locals["T"]


    # Init the output arrays
    X = []
    for d in range(0,dim_s):
        X.append( numpy.zeros(numpy.shape(T[0])) )

    #Algorithmically creating the final nested-for-loop expression depending on "dim_p":
    command = "for d in range(0,dim_s):\n"
    for i in range(0,dim_p):
        command += (i+1)*"    "+"for i"+str(i)+" in range(0,spans["+str(i)+"]+1):\n"
    command += (dim_p+1)*"    "+"X[d] += "
    for i in range(0,dim_p):
        command += "bspline_basis_f(i"+str(i)+",order["+str(i)+"],knot_v["+str(i)+"],T["+str(i)+"]) * "
    command += "control_pts[d"
    for i in range(0,dim_p):
        command += ",i"+str(i)
    command += "]"
    print(command)
    _locals = locals()
    exec(command, globals(), _locals)
    X = _locals["X"]

    # Returning the result
    return X,T




#*** nurbs_spline ***
# dim_p: The parameter-space dimension: 1 = line, 2 = surface, 3 = volume, etc.
# control_pts: Array of control points. Indicies: i,j,k,... where i is over
#    the coordinates of each point, j,k,... are over the spacial directions of the grid.
#    j only for a line; j,k for a surface; j,k,l for a volume; etc.
# weights: An array of weights, one for each control point.  If the weights vector is
#    all ones, then the "nurbs_spline" function should output the same result as the
#    "b_spline" function.
# order:  The order of the curve in each direction.
# spans:  The number of spans between control points in each direction
# dim_s:  The number of spatial dimensions the output will be in. 1 = 1D, 2 = 2D, 3 = 3D, etc.
# knot_v: The array of knot vectors, for each direction.
#    Surface example: numpy.array([[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1]])
# res:  The number of intervals between the beginning and ending t, in each direction
# Returns:
#     X: Array of dimension (dim_s, res[0], res[1],...,res[n]), containing the
#          spatial coordinate locations of the samples of the spline surface.
#     T: Array of dimension (dim_p, res[0], res[1],...,res[n]), containing the
#          parameter-space coordinate locations of the samples the spline surface.
def nurbs_spline(dim_p, control_pts, weights, order, spans, dim_s, knot_v, res):

    # Build the meshgrid, algorithmically creating the expression depending on "dim_p".
    h = []
    t = []
    for i in range(0,dim_p):
        h.append( (numpy.max(knot_v[i]) - numpy.min(knot_v[i])) / float(res[i]-1) )
        t.append( numpy.arange(numpy.min(knot_v[i]), numpy.max(knot_v[i])+0.1*h[i], (1.0-1e-14)*h[i]) )
    if(dim_p > 1):
        command = "T = numpy.meshgrid("
        for i in range(0,dim_p):
            command += "t["+str(i)+"],"
        command += "indexing='xy')"
    else:
        command = "T = [t[0]]"
    print(command)
    _locals = locals()
    exec(command, globals(), _locals)
    T = _locals["T"]

    # Init the output arrays
    X_numer = []
    X_denom = []
    X = []
    for d in range(0,dim_s):
        X_numer.append( numpy.zeros(numpy.shape(T[0])) )
        X_denom.append( numpy.zeros(numpy.shape(T[0])) )
        X.append( numpy.zeros(numpy.shape(T[0])) )

    #Algorithmically creating the final nested-for-loop expression depending on "dim_p":

    #Numerator:
    command = "for d in range(0,dim_s):\n"
    for i in range(0,dim_p):
        command += (i+1)*"    "+"for i"+str(i)+" in range(0,spans["+str(i)+"]+1):\n"
    command += (dim_p+1)*"    "+"X_numer[d] += "
    for i in range(0,dim_p):
        command += "bspline_basis_f(i"+str(i)+",order["+str(i)+"],knot_v["+str(i)+"],T["+str(i)+"]) * "
    command += "weights["
    for i in range(0,dim_p):
        command += "i"+str(i)
        if(i<dim_p-1):
            command += ","
    command += "] * "
    command += "control_pts[d"
    for i in range(0,dim_p):
        command += ",i"+str(i)
    command += "]"
    print(command)
    _locals = locals()
    exec(command, globals(), _locals)
    X_numer = _locals["X_numer"]

    # Denominator:
    command = "for d in range(0,dim_s):\n"
    for i in range(0,dim_p):
        command += (i+1)*"    "+"for i"+str(i)+" in range(0,spans["+str(i)+"]+1):\n"
    command += (dim_p+1)*"    "+"X_denom[d] += "
    for i in range(0,dim_p):
        command += "bspline_basis_f(i"+str(i)+",order["+str(i)+"],knot_v["+str(i)+"],T["+str(i)+"]) * "
    command += "weights["
    for i in range(0,dim_p):
        command += "i"+str(i)
        if(i<dim_p-1):
            command += ","
    command += "]"
    print(command)
    _locals = locals()
    exec(command, globals(), _locals)
    X_denom = _locals["X_denom"]

    # Computing final value:
    command = "for d in range(0,dim_s):\n"
    command += "    "+"X[d] = X_numer[d]/X_denom[d]"
    print(command)
    _locals = locals()
    exec(command, globals(), _locals)
    X = _locals["X"]

    #print("T:",T)
    #print("X_numer:",X_numer)
    #print("X_denom:",X_denom)
    #print("X:",X)

    # Returning the result
    return X,T


# Writes to file at "filename" an obj file of the surface,
# given the real-space sample points in 'X'.
# NOTE: Works for surfaces only, so far.
def write_obj_file(filename,Xlist):

    # Create .obj file.
    f = open(filename, 'w')

    for X in Xlist:
        res = numpy.array(numpy.shape(X[1]))
        # verticies
        for j in range(0,res[0]):
            for i in range(0,res[1]):
                f.write("v %f %f %f\n"%(X[0][i,j],X[1][i,j],X[2][i,j]))
    # faces
    count = 0
    for X in Xlist:
        res = numpy.array(numpy.shape(X[1]))
        for j in range(0,res[0]-1):
            for i in range(0,res[1]-1):
                p1 = count + i*res[0] + j + 1
                p2 = count + i*res[0] + j + 2
                p3 = count + (i+1)*res[0] + j + 2
                p4 = count + (i+1)*res[0] + j + 1
                f.write("f %d %d %d %d\n"%(p1,p2,p3,p4))
                f.write("f %d %d %d %d\n"%(p4,p3,p2,p1))
        count = count + numpy.prod(res)
    f.close()

# TODO: this isn't right yet. Order of i,j,k not quite right maybe.
def write_obj_file_volumes(filename,Xlist):

    # Create .obj file.
    f = open(filename, 'w')

    for X in Xlist:
        res = numpy.array(numpy.shape(X[1]))
        # verticies
        for i in range(0,res[0]):
            for j in range(0,res[1]):
                for k in range(0,res[2]):
                    f.write("v %f %f %f\n"%(X[0][i,j,k],X[1][i,j,k],X[2][i,j,k]))
    # faces
    count = 0
    for X in Xlist:
        res = numpy.array(numpy.shape(X[1]))
        for i in range(0,res[0]-1):
            for j in range(0,res[1]-1):
                for k in range(0,res[2]-1):
                    p1 = count + k*res[0]*res[1] + i*res[0] + j + 1
                    p2 = count + k*res[0]*res[1] + i*res[0] + j + 2
                    p3 = count + k*res[0]*res[1] + (i+1)*res[0] + j + 1
                    p4 = count + k*res[0]*res[1] + (i+1)*res[0] + j + 2
                    p5 = count + (k+1)*res[0]*res[1] + i*res[0] + j + 1
                    p6 = count + (k+1)*res[0]*res[1] + i*res[0] + j + 2
                    p7 = count + (k+1)*res[0]*res[1] + (i+1)*res[0] + j + 1
                    p8 = count + (k+1)*res[0]*res[1] + (i+1)*res[0] + j + 2
                    # face 1
                    f.write("f %d %d %d %d\n"%(p1,p2,p4,p3))
                    f.write("f %d %d %d %d\n"%(p3,p4,p2,p1))
                    # face 2
                    f.write("f %d %d %d %d\n"%(p5,p6,p8,p7))
                    f.write("f %d %d %d %d\n"%(p7,p8,p6,p5))
                    # face 3
                    f.write("f %d %d %d %d\n"%(p2,p4,p8,p6))
                    f.write("f %d %d %d %d\n"%(p6,p8,p4,p2))
                    # face 4
                    f.write("f %d %d %d %d\n"%(p1,p3,p7,p5))
                    f.write("f %d %d %d %d\n"%(p5,p7,p3,p1))
                    # face 5
                    f.write("f %d %d %d %d\n"%(p1,p2,p6,p5))
                    f.write("f %d %d %d %d\n"%(p5,p6,p2,p1))
                    # face 6
                    f.write("f %d %d %d %d\n"%(p3,p4,p8,p7))
                    f.write("f %d %d %d %d\n"%(p7,p8,p4,p3))
        count = count + numpy.prod(res)
    f.close()
